# README #

1/3スケールのPanasonic FS-A1風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはFusion360です。

***

# 実機情報

## メーカ
- Panasonic
- 松下電器産業

## 発売時期
- 1986年

## 参考資料

- [ボクたちが愛した、想い出のレトロパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1270903.html)
- [MSX wiki](https://www.msx.org/wiki/Panasonic_FS-A1)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fs-a1/raw/479962c70d9c8c11edd51c5ce79b2a89e57cd667/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fs-a1/raw/479962c70d9c8c11edd51c5ce79b2a89e57cd667/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fs-a1/raw/479962c70d9c8c11edd51c5ce79b2a89e57cd667/ExampleImage.jpg)
